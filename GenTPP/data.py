import numpy as np
import sympy as sy
import torch

import math
import random

from utils import *




# homogeneous Poisson process
def create_hom_poisson(mean, n_samples = 100, max_T = 100, min_seq_len = 2):
  samples = []
  i = 0
  while True:
    seq = []
    t = 0
    while True:
      intens1 = mean
      dt = np.random.exponential(1/intens1)
      new_t = t + dt
      if new_t > max_T:
        break
        
      intens2 = mean
      u = np.random.uniform()
      if intens2/intens1 >= u:
        seq.append(new_t)
      t = new_t
    if len(seq)>1:
      samples.append(seq) 
      i+=1
    if i==n_samples:
      break

  samples = [seq for seq in samples if len(seq)>min_seq_len]

  return samples


# Hawkes process (github.com/stmorse/hawkes)
class HawkesProcess:
  def __init__(self, params={'alpha': 0.8,'mu': 0.1, 'omega': 1.0}):
    self.data = []
    self.alpha, self.mu, self.omega = params['alpha'], np.array(params['mu']), params['omega']
    self.dim = 1

  def generate_seq(self, horizon):
    self.data = []

    Istar = np.sum(self.mu)
    s = np.random.exponential(scale=1./Istar)

    n0 = 0
    self.data.append([s, n0])

    lastrates = self.mu

    decIstar = False
    while True:
      tj, uj = self.data[-1][0], int(self.data[-1][1])

      if decIstar:
        Istar = np.sum(rates)
        decIstar = False
      else:
        Istar = np.sum(lastrates) + self.omega * self.alpha

      s += np.random.exponential(scale=1./Istar)
      rates = 0.1 + np.exp(-self.omega * (s - tj)) * (self.alpha * self.omega + lastrates - 0.1)

      diff = Istar - np.sum(rates)

      n0 = np.random.choice(np.arange(1+1), 1, p=(np.append(rates, diff) / Istar))

      if n0 < 1:
        self.data.append([s, n0])
        lastrates = rates.copy()
      else:
        decIstar = True

      if s >= horizon:
        self.data = np.array(self.data)
        self.data = self.data[self.data[:,0] < horizon]
        return list(self.data[:,0])

def create_hawkes(params = {'alpha': 0.8,'mu': 0.1, 'omega': 1.0}, n_samples=10, max_T=20, min_seq_len = 2):
  hawkes_process = HawkesProcess(params=params)
  samples = [hawkes_process.generate_seq(max_T) for _ in range(n_samples)]
  samples = [seq for seq in samples if len(seq)>min_seq_len]

  return samples

def get_residuals_hawkes(sequences, params = {'alpha': 0.8,'mu': 0.1, 'omega': 1.0}, max_T=20):
  integrals = []
  correlation = []
  alpha = params['alpha']
  mu = params['mu']

  for seq in sequences:
    seq = np.sort(seq)
    residuals = []

    for i in range(len(seq)-1):
      residual = (seq[i+1]-seq[i])*mu + alpha * np.sum(np.exp(-(seq[i]-seq[:i+1]))-np.exp(-(seq[i+1]-seq[:i+1])))
      residuals.append(residual)

    residuals = residuals
    integrals += residuals

  return np.array(integrals)


# inhomogeneous Poisson process
def create_inhom_poisson(func_expr, n_samples = 100, max_T = 100, min_seq_len = 2):
  samples = []

  x = sy.Symbol("x")
  f = sy.lambdify(x, func_expr, "numpy")

  for i in range(n_samples):
    curr_sample = []
    index = []
    t = 0

    while t<max_T:
      intens1 = np.max(f(np.arange(t, max_T, 0.1)))
      dt = np.random.exponential(1/intens1)
      new_t = t + dt
      if new_t > max_T:
        break

      intens2 = f(new_t)
      u = np.random.uniform()
      if intens2/intens1 >= u:
        curr_sample.append(new_t)
      t = new_t

    samples.append(curr_sample)

  samples = [seq for seq in samples if len(seq)>min_seq_len]

  return samples

def get_residuals_inhom_poisson(sequences, func_expr, max_T=100):
  integrals = []
  correlations = []

  x = sy.Symbol("x")
  f_int = sy.integrate(func_expr, x)
  f_int = sy.lambdify(x, f_int, "numpy")

  for seq in sequences:
    seq = np.sort(seq)

    int_values = f_int(seq)

    residuals = int_values[1:] - int_values[:-1]
    residuals = residuals.tolist()
    integrals += residuals

  return np.array(integrals)


# inhomogeneous Poisson process with gaussian kernels as intensity function
class IntensitySumGaussianKernel:
  def __init__(self, k=2, centers=[2, 4], stds=[1, 1], coefs= [1, 1]):
    self.k = len(centers)
    self.centers = centers
    self.stds = stds
    self.coefs = coefs
  def getValue(self, t):
    inten = 0
    for i in range(self.k):
      inten += self.coefs[i]*(np.exp(-(t-self.centers[i])**2/(2*self.stds[i]**2))/np.sqrt(2*np.pi*self.stds[i]**2))
    return inten
  def getUpperBound(self, from_t, to_t):
    max_val = max(self.getValue(from_t), self.getValue(to_t))
    for i in range(self.k):
      max_val = max(max_val, self.getValue(self.centers[i]))
    for i in range(self.k-1):
      point = (self.coefs[i]*self.centers[i]/self.stds[i] + self.coefs[i+1]*self.centers[i+1]/self.stds[i+1])/(self.coefs[i]/self.stds[i] + self.coefs[i+1]/self.stds[i+1])
      max_val = max(max_val, self.getValue(point))
    return max_val

def create_inhom_poisson_gauss_kernels(intens_gauss_kernels, n_samples = 100, max_T = 100):
  sequences = []
  i = 0
  while True:
    seq = []
    t = 0
    while True:
      intens1 = intens_gauss_kernels.getUpperBound(t, max_T)
      dt = np.random.exponential(1/intens1)
      new_t = t + dt
      if new_t > max_T:
        break
        
      intens2 = intens_gauss_kernels.getValue(new_t)
      u = np.random.uniform()
      if intens2/intens1 >= u:
        seq.append(new_t)
      t = new_t
    if len(seq)>1:
      sequences.append(seq) 
      i+=1
    if i==n_samples:
      break
  return sequences

def get_residuals_inhom_poisson_gauss_kernels(sequences, params):
  integrals = []
  n_kernels = len(params['coef'])
  for seq in sequences:
    integral = []
    seq = np.asarray(seq)
    seq = np.sort(seq)
    for i in range(len(seq)-1):
      integral_delta = 0
      for j in range(n_kernels):
        # integral_delta += np.sum( params['coef'][j] * (stats.norm.cdf(seq[i+1], params['center'][j], params['std'][j]) - stats.norm.cdf(seq[i], params['center'][j], params['std'][j]) ) )
        integral_delta += np.sum( params['coef'][j]) * (normalcdf(seq[i+1], params['center'][j], params['std'][j]) - (normalcdf(seq[i], params['center'][j], params['std'][j]) ) )
      integral.append(integral_delta)
    integrals+=integral
  return np.array(integrals)

# iterator to create batches of sequences which are padded to maximum sequence length in batch
class PaddedDataIterator:
  def __init__(self, sequences, batch_size, pad_elem = -1, sort_by_len = True):
    self.sort_by_len = sort_by_len
    self.sequences = sequences
    self.length = [len(item) for item in self.sequences]
    self.batch_size = batch_size
    self.size = len(self.sequences)
    self.cursor = 0
    self.pad_elem = pad_elem

  def shuffle(self):
    random.shuffle(self.sequences)
    self.length = [len(item) for item in self.sequences]
    self.cursor = 0

  def next_batch(self):
    if self.cursor+self.batch_size > self.size:
      self.shuffle()
    res = self.sequences[self.cursor:self.cursor + self.batch_size]
    seqlen = self.length[self.cursor:self.cursor + self.batch_size]
    self.cursor += self.batch_size

    if self.sort_by_len:
      res.sort(key=lambda s: len(s), reverse=True)
      seqlen.sort(reverse=True)

    max_len = max(seqlen)
    x = np.ones([self.batch_size, max_len, 1], dtype=np.float32) * self.pad_elem
    for i, x_i in enumerate(x):
      x_i[:seqlen[i],0] = res[i]

    return x, np.asarray(seqlen)


# create data used for the diagnostic plots and potentially other plots
class CreateValidationData:
  def __init__(self, seqs_noise, seqs_real, n_seqs, device):
    self.device = device
    self.n_seqs = min(n_seqs, min(len(seqs_noise), len(seqs_real)))

    self.seqs_noise = seqs_noise[:n_seqs]
    self.seqs_real = seqs_real[:n_seqs]

    iterator_noise = PaddedDataIterator(self.seqs_noise, self.n_seqs, sort_by_len=True)
    iterator_real  = PaddedDataIterator(self.seqs_real, self.n_seqs, sort_by_len = True)

    noise_seqs, noise_seqs_lens = iterator_noise.next_batch()
    real_seqs, real_seqs_lens  = iterator_real.next_batch()

    self.batch_noise_seqs = torch.Tensor(noise_seqs).to(self.device)
    self.batch_noise_seqs_lens = torch.Tensor(noise_seqs_lens).to(self.device)

    self.batch_real_seqs  = torch.Tensor(real_seqs).to(self.device)
    self.batch_real_seqs_lens = torch.Tensor(real_seqs_lens).to(self.device)


# Get emperical intensity of sequences
def get_intensity(sequences, T = None, n_t = None, t0 = None):
  if T is None:
    T = max([max(seq) for seq in sequences])
  if n_t is None:
    n_t = 50
  if t0 is None:
    t0 = 0

  dt = (T-t0)/n_t
  ts = np.arange(t0,T,dt)
  n_seqs = len(sequences)
  lens = np.zeros((n_seqs,1))
  cnt = np.zeros((n_t,1))

  for i in range(n_seqs):
    seq = sequences[i]
    j = 0
    k = 0
    for t in np.arange(t0+dt,T+dt,dt):
      while (j < len(seq) and seq[j] <= t):
        j = j + 1
      cnt[k] = cnt[k] + j
      k = k + 1

  dif = np.zeros((len(cnt),1))
  dif[0] = cnt[0]
  for i in range(len(cnt)-1):
    dif[i+1] = cnt[i+1]-cnt[i]
  intensity = dif/(n_seqs)/dt

  return ts, intensity 

