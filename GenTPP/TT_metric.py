import numpy as np
import sympy as sy
import matplotlib.pyplot as plt

import argparse
from itertools import combinations

from data import (create_hom_poisson,
                  create_hawkes,
                  create_inhom_poisson,
                  create_inhom_poisson_gauss_kernels,
                  IntensitySumGaussianKernel)



def nearest_neighbor(X, Y):
  # compute nearest neighbour assignment from X to Y (assumes seq. len. of X <= seq. len. of Y)

  m = len(X)
  assignment = []

  for i in range(m):
    nn_ind = np.argmin(np.abs(X[i] - Y))
    assignment.append(nn_ind)

  return assignment


def partial_optimal_assignment(X, Y, penalty = 20):
  # compute partial optimal assignment between sequences X and Y and corresponding TT-metric value

  m = len(X)
  n = len(Y)

  X = np.array(X)
  Y = np.array(Y)

  if m>n:
    X, Y = Y, X
    m, n = n, m

  a = []

  t = nearest_neighbor(X, Y)
  a.append(t[0])

  for m_ in range(m-1): # loop starts at 0

    if t[m_+1] > a[m_]:
      # Case of no collision 
      a.append(t[m_+1])
    
    else:    
      # retrieve r (s not needed)

      a_range = list(np.arange(a[0], a[m_]+1))
      a_unassigned = [el for el in a_range if not el in a]  # unassigned points in range of a

      if len(a_unassigned) == 0:
        r = 0

      else:
        last_unassigned = a_unassigned[-1]
        a_ = [el for el in a if el<last_unassigned]
        r = len(a_)

      if a[r] > 0:
        # Case that we do have points to the left of Y[a_range]

        a_shifted = [el-1 for el in a]

        if a[-1] == n-1:
          # Case that no points of on the right of Y[a_range]

          a[r:(m_+1)] = a_shifted[r:(m_+1)]
          a.append(a[m_]+1)

        else:
          w1 = np.sum( np.square( X[r:(m_+1)] - np.take(Y, a_shifted[r:(m_+1)]) ) ) + np.square( X[m_+1] - Y[a[m_]] )
          w2 = np.sum( np.square( X[r:(m_+1)] - np.take(Y, a[r:(m_+1)]) ) ) + np.square( X[m_+1] - Y[a[m_]+1] )

          if w1 < w2:
            # Case 1 in paper

            a[r:(m_+1)] = a_shifted[r:(m_+1)]
            a.append(a[m_]+1)

          else:
            # Case 2 in paper

            a.append(a[m_]+1)

      else:
        # Case that no points to the left of Y[a_range]

        a.append(a[m_]+1)

  # compute corresponding TT-metric value
  dist = np.sum(   (X - np.take(Y, a))**2   )
  dist = dist + (n-m) * penalty**2

  dist = np.sqrt(dist)

  return a, dist


def assignment_to_transport_plan(pOA_plan, max_seq_len):
  # transform partial optimal assignment pOA_plan to transport plan
  # each assignment i -> j in pOA_plan yields weight of 1/max_seq_len in transport plan matrix
  # at matrix element (i,j) (matrix elements zero otherwise)

  plan = np.zeros((max_seq_len, max_seq_len))

  l_ = len(pOA_plan)

  for u in range(max_seq_len):
    for v in range(max_seq_len):
      if u<l_:
        if pOA_plan[u] == v:
          plan[u][v] = 1/max_seq_len

  return plan


def approximate_transport_plan(seq1, seq2, algo_type, beta=10, n_sink=5, n_ipot=1, penalty=20):
  """ 
  Calculate the approximative transport plan that corresponds to the TT-metric when it is formulated
  as a primal optimal transport problem. The cost matrix is given by the pairwise quadratic
  distances between the sequence elements and the penalty value.

  Args:
    - seq1, seq2: sequences (sequence length of seq1 assumed to be smaller or equal than that of seq2)
        according to which the approximative transport plan is calculated

  Parameters:
    - algo_type: use the Sinkhorn distance or IPOT algorithm for the approximation of the transport plan
    - beta: regularization parameter in Sinkhorn distance or IPOT algorithm
    - n_sink: number of Sinkhorn iterations, corresponds to inner iterations if approx = "ipot"
    - n_ipot: number of main iterations in IPOT algorithm, ignored when approx = "sink"

  Return:
    - T: approximation of transport plan
    - C: cost matrix of the primal optimal transport problem formulation of TT-metric
    - TT_approx: approximation of TT-metric between sequences via the transport plan approximation

  """

  X = np.array(seq1)
  Y = np.array(seq2)

  X_len = len(seq1)
  Y_len = len(seq2)

  length_ = Y_len

  X = np.expand_dims(X, axis=1) # (X_len, 1)
  Y = np.expand_dims(Y, axis=0) # (1, Y_len)

  C = penalty * np.ones((length_, length_))
  C[:X_len, :] = X - Y
  C = C**2

  T = np.ones((length_, length_))
  b = np.ones((length_, 1))
  A = np.exp(-C/beta)
  
  if algo_type == "sink":
    Q = A
    QT = np.transpose(Q)
    for i in range(n_sink):
      a = 1/(length_ * np.matmul(Q, b))
      b = 1/(length_ * np.matmul(QT, a))

    a_diag = np.diag(np.squeeze(a))
    b_diag = np.diag(np.squeeze(b))

    T = np.matmul(a_diag, Q)
    T = np.matmul(T, b_diag)    

  if algo_type == "ipot":
    for i in range(n_ipot):
      Q = A*T
      QT = np.transpose(Q)

      for j in range(n_sink):
        a = 1/(length_ * np.matmul(Q, b))
        b = 1/(length_ * np.matmul(QT, a))

      a_diag = np.diag(np.squeeze(a))
      b_diag = np.diag(np.squeeze(b))

      T = np.matmul(a_diag, Q)
      T = np.matmul(T, b_diag)

  TT_approx = T*C
  TT_approx = np.sum(TT_approx)

  TT_approx = np.sqrt(length_*TT_approx)

  return T, C, TT_approx


def plot_transport_plan_approxs(X, Y, max_T, algo_list, reg_params_list, main_iters, size=4):
  # plot transport plan approximation corresponding to solution of primal OT formulation of TT-metric
  # between sequence X and sequence Y
  # plotted for each algorithm in algo_list and each regularization parameter in reg_params_list

  n_algos = len(algo_list)
  n_reg_params = len(reg_params_list)
  max_seq_len = len(Y)
  size_hor = size*n_algos
  size_ver = size*n_reg_params

  fontsize = size*3
  fig, axs = plt.subplots(n_reg_params, n_algos, figsize=(size_hor, size_ver), facecolor='w', edgecolor='k')

  for i in range(n_reg_params):
    beta = reg_params_list[i]

    for j in range(n_algos):
      algo = algo_list[j]
      transport_plan = approximate_transport_plan(X, Y, algo_type=algo["algo"], beta=beta, n_sink=algo["n_sink"], n_ipot=algo["n_ipot"], penalty=max_T/2)[0]

      vmax = 1/max_seq_len
      axs[i,j].imshow(transport_plan, cmap='Blues', vmin=0, vmax=vmax)
      axs[i,j].set_xticks(np.arange(0, max_seq_len, 2))
      axs[i,j].set_yticks(np.arange(0, max_seq_len, 2))
      axs[i,j].set_xticklabels(np.arange(1, max_seq_len+1, 2))
      axs[i,j].set_yticklabels(np.arange(1, max_seq_len+1, 2))
      axs[i,j].set_xticks(np.arange(-.5, max_seq_len, 1), minor=True)
      axs[i,j].set_yticks(np.arange(-.5, max_seq_len, 1), minor=True)

      axs[i,j].grid(which='minor', color='gray', linestyle='-', linewidth=1, alpha=0.1)

      if i < (n_reg_params-1):
        axs[i,j].set_xticks([])

      str_reg_parms = "($\rho$ = " + ", ".join([str(p) for p in reg_params_list]) + ")"
      if i == 0 and algo_list[j]["algo"] == "sink":
        str_label = "Sinkhorn " + str_reg_parms
        axs[i,j].set_xlabel(repr(str_label), fontsize=fontsize, labelpad=10)
        axs[i,j].xaxis.set_label_position('top')
      if i == 0 and algo_list[j]["algo"] == "ipot":
        str_label = "IPOT, " + "n_sink = " + str(algo_list[j]["n_sink"]) + " " + str_reg_parms
        axs[i,j].set_xlabel(repr(str_label), fontsize=fontsize, labelpad=10)
        axs[i,j].xaxis.set_label_position('top')

  plt.show()


def plot_exact_transport_plan_and_assignment(X, Y, max_T, print_assignment=False, size=4):
  size = size
  fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(3*size, size), facecolor='w', edgecolor='k')

  max_seq_len = len(Y)
  optimal_assignment = partial_optimal_assignment(X, Y)[0]
  exact_transport_plan = assignment_to_transport_plan(optimal_assignment, max_seq_len=max_seq_len)

  # Plot exact transport plan given by optimal assignment
  vmax = 1/max_seq_len
  ax_plot = axs[0].imshow(exact_transport_plan, cmap='Blues', vmin=0, vmax=vmax)
  fig.colorbar(ax_plot, ax=axs[0])

  axs[0].set_xticks(np.arange(0, max_seq_len, 2))
  axs[0].set_yticks(np.arange(0, max_seq_len, 2))
  axs[0].set_xticklabels(np.arange(1, max_seq_len+1, 2))
  axs[0].set_yticklabels(np.arange(1, max_seq_len+1, 2))
  axs[0].set_xticks(np.arange(-.5, max_seq_len, 1), minor=True)
  axs[0].set_yticks(np.arange(-.5, max_seq_len, 1), minor=True)

  axs[0].grid(which='minor', color='gray', linestyle='-', linewidth=1, alpha=0.1)

  axs[0].set_xlabel(r'True transport plan', fontsize=5*size, labelpad=10)
  axs[0].xaxis.set_label_position('top')


  # Plot the underlying sequences
  markersize = 8
  marker_style_unmatched = {'color': 'black', 'marker': 'o', 'fillstyle': 'none', 'markersize': markersize}

  axs[1].axhline(1, color='black')
  axs[1].axhline(-1, color='black')

  axs[1].plot(X, 1*np.ones_like(X), **marker_style_unmatched)
  axs[1].plot(Y, -1*np.ones_like(Y), **marker_style_unmatched)

  for i in range(0, len(optimal_assignment)):
    axs[1].plot([X[i], Y[optimal_assignment[i]]], [1, -1], **marker_style_unmatched)
    axs[1].annotate(s='', xy=(Y[optimal_assignment[i]],-1), xytext=( X[i],1), arrowprops=dict(arrowstyle='->'), fontsize =20) 

  axs[1].set_xlim([0, max_T])
  axs[1].set_ylim([-1.4, 2])

  xticks = axs[1].get_xticks()
  axs[1].set_xticks(xticks)
  axs[1].set_xticklabels(xticks)

  axs[1].set_frame_on(False)
  axs[1].axes.get_yaxis().set_visible(False)

  xmin, xmax = axs[1].get_xaxis().get_view_interval()
  ymin, ymax = axs[1].get_yaxis().get_view_interval()
  axs[1].add_artist(plt.Line2D((xmin, xmax+2), (ymin, ymin), color='black', linewidth=2))

  # axs[1].text(-0.6, 1, s=r'shorter sequence', fontsize=16, horizontalalignment='center', verticalalignment='center')
  # axs[1].text(-0.6, -1, s=r'longer sequence', fontsize=16, horizontalalignment='center', verticalalignment='center')

  if print_assignment == True:
    print("optimal assignment from X indices to Y indices: ", optimal_assignment)

  plt.show()


def plot_intens_funcs(list_intens_funcs, max_T):
  # list_intens_funcs: list of symbolic function expressions (using sympy)
  size = 4
  plt.figure(figsize=(3*size, size))
  plt.xlim(0, max_T)
  plt.tick_params(axis='both', which='major', labelsize=22)

  ls_list = ["-",":","-.","--"]

  x_vals = np.arange(0, max_T, 0.1)

  max_val = 0
  for i, intens_func in enumerate(list_intens_funcs):
    x = sy.Symbol("x")
    intens_func_ = sy.lambdify(x, intens_func, "numpy")
    intens_func_vals = intens_func_(x_vals)
    max_val_curr = np.max(intens_func_vals)
    max_val = max(max_val, max_val_curr)
    plt.plot(x_vals, intens_func_vals, linestyle=ls_list[min(i, len(ls_list))], color="black", linewidth=2, label=r"Intens. func. %s" %(i+1))

  plt.ylim(0, 1.3*max_val)
  plt.legend(loc="upper right", labelspacing=1)
  plt.show()


def calculate_MAPEs_between_exact_approx_TT(sequences_list, algo_list, iters, max_T, penalty):
  # for each algorithm calculate MAPE between exact and approximated TT-metric based on respectively
  # two sets of sequences (respectively from all unique pairs of sequence sets in sequences_list)

  n_seqs = len(sequences_list)
  unique_seqs_pairs = list(combinations(range(n_seqs), 2))

  for (i,j) in unique_seqs_pairs:
    seqs_1 = sequences_list[i]
    seqs_2 = sequences_list[j]
    print("Calculate MAPEs for sequences %s and %s." %(i+1,j+1))

    # exact TT-metric
    TT_exact = []
    n_samples = min(len(seqs_1), len(seqs_2))
    for i in range(n_samples):
      X, Y = seqs_1[i], seqs_2[i]
      if len(X) > len(Y):
        Y, X = X, Y

      exact_TT_dist = partial_optimal_assignment(X, Y, penalty=penalty)[1]
      TT_exact.append(exact_TT_dist)

    # TT-metric approximations
    for algo in algo_list:
      algo_type = algo["algo"]
      n_sink = algo["n_sink"]
      n_ipot = algo["n_ipot"]
      rho_list = algo["reg_params"]

      MAPEs = []
      for j, rho in enumerate(rho_list):
        TT_approxs = []
        for i in range(n_samples):
          X, Y = seqs_1[i], seqs_2[i]
          if len(X) > len(Y):
            Y, X = X, Y

          approx_TT_value = approximate_transport_plan(X, Y, algo_type=algo_type, beta=rho, n_sink=n_sink, n_ipot=n_ipot, penalty=penalty)[2]
          TT_approxs.append(approx_TT_value)

        MAPE = np.mean(   np.abs(   (np.array(TT_approxs) - np.array(TT_exact))/np.array(TT_exact)    )     )
        MAPEs.append(np.round(MAPE, 5))

      if algo_type == "sink":
        print('MAPEs with Sinkhorn algorithm (rho = {}): {:>51}'.format(str(tuple(rho_list)), str(tuple(MAPEs))))
      if algo_type == "ipot":
        print('MAPEs with IPOT algorithm with {:>2} Sinkhorn iterations (rho = {}): {}'.format(n_sink, str(tuple(rho_list)), str(tuple(MAPEs))))
    print('\n')




if __name__ == "__main__":


	X = [1.94,  5.26,  6.12,  6.83,  7.71,  9.68, 11.85, 12.23, 18.57, 20.21, 21.86, 22.66, 25.86, 26.01, 26.43]
	Y = [1.25,  1.75,  4.5 ,  5.52,  5.81,  6.75,  7.92, 12.9 , 14.55, 17.02, 17.82, 21.48, 22.67, 22.68, 23.42, 24.74, 26.13, 26.15, 26.33, 28.03]
	X_len = 15
	Y_len = 20

	max_seq_len = max(len(X), len(Y))
	max_T = 30
	penalty = max_T/2  # penalty for unmatched points in Sinkhorn and IPOT algorithm


	# for every algorithm in list plot transport plan with the given list of regularization parameters
	main_iters = 100
	reg_params_list = [20, 40, 60] # regularization parameters (same for both algorithms)
	algo_list = [{"algo": "sink", "n_sink": main_iters, "n_ipot": None},
	             {"algo": "ipot", "n_sink": 1,          "n_ipot": main_iters},
	             {"algo": "ipot", "n_sink": 10,         "n_ipot": main_iters}]

	plot_transport_plan_approxs(X, Y, max_T, algo_list=algo_list, reg_params_list=reg_params_list, main_iters=main_iters, size=3)


	plot_exact_transport_plan_and_assignment(X, Y, max_T, print_assignment=False, size=4)


	# define and plot intensity functions of three inhom. Poisson processes
	x = sy.Symbol("x")
	intens_func_1 = 0.8 + 0.000000000000000000001*x
	intens_func_2 = 1.1 + sy.sin(sy.pi*(x)/5 - sy.pi/2)
	intens_func_3 = 150*(x/30)**2 * (1-(x/30))**5

	plot_intens_funcs(list_intens_funcs=[intens_func_1, intens_func_2, intens_func_3], max_T=max_T)


	# calculate MAPE between exact TT-metric value and its approximation by the algorithms where we
	# evaluate the exact TT-metric value and its approximation over pairs of sequence samples
	max_T = 30
	penalty = max_T/2 # penalty of TT-metric
	n_samples = 100

	# generate samples and calculate the MAPE between all pairs of these samples
	sequences_1 = create_inhom_poisson(func_expr = intens_func_1, n_samples=n_samples, max_T=30)
	sequences_2 = create_inhom_poisson(func_expr = intens_func_2, n_samples=n_samples, max_T=30)
	sequences_3 = create_inhom_poisson(func_expr = intens_func_3, n_samples=n_samples, max_T=30)

	sequences_list = [sequences_1, sequences_2, sequences_3]

	# specify the algorithms for the TT-metric approximation and a list of regularization parameters
	rho_list  = [20, 40, 60]
	algo_list = [{"algo": "sink", "n_sink": main_iters, "n_ipot": None,       "reg_params": rho_list},
	             {"algo": "ipot", "n_sink": 1,          "n_ipot": main_iters, "reg_params": rho_list},
	             {"algo": "ipot", "n_sink": 10,         "n_ipot": main_iters, "reg_params": rho_list}]
	iters     = 50 # number of main iterations of algorithms

	print("Calculate mean absolute percentage error (MAPE) between exact TT-metric value")
	print("and its approximation for 100 pairs of sequences.")
	print("")
	calculate_MAPEs_between_exact_approx_TT(sequences_list=sequences_list, algo_list=algo_list, iters=500, max_T=max_T, penalty=penalty)






